let startButton = document.getElementById("submit")
let showOnStart = document.getElementById("date")
let stopButton = document.getElementById("submit1")
let clearButton = document.getElementById("submit2")
let showOnStop = document.getElementById("stop")
let showMinute = document.getElementById("minute")
let showMoney = document.getElementById("money")


var myVar = setInterval(myTimer, 1000);
function myTimer() {
  var d = new Date()
  var t = d.toString().split(" ")
  var date = `${t[0]}`+" "+`${t[1]}`+"\t"+`${t[2]}`+"\t"+`${t[3]}`+"\t"+d.toLocaleTimeString()
  document.getElementById("showTime").innerHTML = date
}
startButton.onclick = () =>{
  
    let startDate = new Date()
    var startTime = startDate.toLocaleTimeString().toString().split(":")
    let clearStartMinute = startTime[2].split(" ")
    let timeToStart = `${startTime[0]}`+":"+`${startTime[1]}`+" "+clearStartMinute[1]
    showOnStart.innerHTML= timeToStart
    document.getElementById("a").style.display = 'none'
    document.getElementById("b").style.display = "block"
    console.log(startTime)
}
stopButton.onclick = () =>{
    let stopDate = new Date() 
    var stopTime = stopDate.toLocaleTimeString().toString().split(":")
    let clearStopMinute = stopTime[2].split(" ")
    let TimeToStop  = `${stopTime[0]}`+":"+`${stopTime[1]}`+" "+clearStopMinute[1]
    showOnStop.innerHTML= TimeToStop

    document.getElementById("b").style.display = 'none'
    document.getElementById("c").style.display = "block"
    let startValue = document.getElementById("date").innerHTML.split(":")
    let startMinuteFormat = startValue[1].toString().split(" ");
    let startMinute = (startValue[0]*60)+startMinuteFormat[0]
    let stopMinute  = (stopTime[0]*60)+stopTime[1]
    let time = stopMinute - startMinute
    showMinute.innerHTML = time

    if (time <= 15) {
      showMoney.innerHTML = 500
  } else if (time <= 30) {
    showMoney.innerHTML = 1000
  } else if (time <= 60) {
    showMoney.innerHTML = 1500
  } else if (time > 60) {
      if (time % 60 == 0) {
          let a = time / 60
          showMoney.innerHTML = Math.floor(a) * 1500
      } else if (time % 60 <= 15) {
          let a = time / 60
          showMoney.innerHTML = Math.floor(a) * 1500 + 500
      } else if (time % 60 <= 30) {
          let a = time / 60
          showMoney.innerHTML = Math.floor(a) * 1500 + 1000
      } else if (time % 60 <= 60) {
          let a = time / 60
          showMoney.innerHTML = Math.floor(a) * 1500 + 1500
      }
  }
    
}
clearButton.onclick = () =>{
  document.getElementById("c").style.display = 'none'
  document.getElementById("a").style.display = "block"
  showOnStart.innerHTML= "0.00"
  showOnStop.innerHTML= "0.00"
  showMinute.innerHTML = "0"
  showMoney.innerHTML = "0"
}

